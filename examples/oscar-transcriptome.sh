#!/bin/bash
#SBATCH -t 48:00:00
#SBATCH -c 16
#SBATCH --mem=50G

set -e


# INSERT AN ID FOR YOUR DATASET AND PATHS TO THE FORWARD/REVERSE READ FILES #
ID=
FASTQ1=
FASTQ2=


module load agalma

unset BIOLITE_CONFIG
export BIOLITE_RESOURCES="database=$PWD/biolite.sqlite,agalma_database=$PWD/agalma.sqlite,outdir=$PWD,threads=${SLURM_CPUS_ON_NODE},memory=${SLURM_MEM_PER_NODE}M"

agalma catalog insert --path $FASTQ1 $FASTQ2 --id $ID

agalma transcriptome --id $ID

