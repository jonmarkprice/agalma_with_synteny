#!/usr/bin/env python

import sys
from biolite import diagnostics

hists = []

for run_id in sys.argv[1:]:
	values = diagnostics.lookup(run_id, 'homologize.load_mcl_cluster')
	hists.append(eval(values['histogram']))

keys = set()
map(keys.update, hists)

for key in sorted(keys):
	columns = [key]
	for hist in hists:
		columns.append(hist.get(key, '0'))
	print '\t'.join(map(str, columns))
